package pl.exaco.internship.android.weatherdemo.service.api;

import pl.exaco.internship.android.weatherdemo.model.FutureWeather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherForecastApi {

    /**
     * URL = "forecast"
     * query param - id - example: 1
     * <p>
     * return - FutureWeather
     * @param id
     */

    @GET("forecast")
    Call<FutureWeather> getWeatherForecast(@Query("id") String id);
}
