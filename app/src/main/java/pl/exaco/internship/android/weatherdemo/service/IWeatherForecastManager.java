package pl.exaco.internship.android.weatherdemo.service;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.City;
import pl.exaco.internship.android.weatherdemo.model.CityWeather;

public interface IWeatherForecastManager {

    void getWeatherForecastForCity(int cityId, RequestCallback<List<CityWeather>> callback);
}
