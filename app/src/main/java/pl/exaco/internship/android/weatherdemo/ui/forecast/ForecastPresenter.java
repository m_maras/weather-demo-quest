package pl.exaco.internship.android.weatherdemo.ui.forecast;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.City;
import pl.exaco.internship.android.weatherdemo.model.CityWeather;
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory;
import pl.exaco.internship.android.weatherdemo.service.IWeatherForecastManager;
import pl.exaco.internship.android.weatherdemo.service.RequestCallback;

public class ForecastPresenter implements ForecastContract.Presenter {

    private final ForecastContract.View view;
    private final IWeatherForecastManager weatherForecastManager;

    public ForecastPresenter(ForecastContract.View view, IServiceFactory serviceFactory) {
        this.view = view;
        this.weatherForecastManager = serviceFactory.getWeatherForecastManager();
    }

    @Override
    public void getForecast(int cityId) {
        weatherForecastManager.getWeatherForecastForCity(cityId, new RequestCallback<List<CityWeather>>() {
            @Override
            public void onSuccess(List<CityWeather> data) {
                view.onSuccess(data);
            }

            @Override
            public void onError(Throwable throwable) {
                view.onFailure();
            }
        });
    }
}
